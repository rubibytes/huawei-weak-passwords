# Huawei Weak Passwords

## Description
A .txt file containing "weak" passwords originally published [here](https://support.huawei.com/enterprise/en/doc/EDOC1100194277/6512e873/weak-password-dictionary#en-us_topic_0000001130537715_en-us_topic_0000001129985307_table175956194175).

I copied the list from the webpage to a .txt file. I used a Python script to write all the passwords to a new file ignoring all empty lines.

## Authors and acknowledgment
Special thanks to Huawei for helping raise awareness of weak passwords.

## License
At the time of writing, this list did not have any guidelines for use and attribution. This list is for information only.

